package cn.turing.firecontrol.common.constant;

/**
 * @Description TODO
 * @Author gongxuan
 * @Date 2020/05/28 14:38
 * @Version V3.2
 */
public class MessageConst {

    /**
     * 本地短信和电话服务的服务地址
     */
    public static String LOCAL_SERVER_URL = "http://192.168.0.200:80/cgi-bin/NoticePush";

    /**
     * 本地服务要发送到的地址字段
     */
    public static String LOCAL_SERVER_TO = "To";

    /**
     * 本地服务的服务类型字段
     */
    public static String LOCAL_SERVER_TYPE = "Type";

    /**
     * 本地服务的编码字段
     */
    public static String LOCAL_SERVER_ENCODING = "Encoding";

    /**
     * 本地服务的文本字段
     */
    public static String LOCAL_SERVER_Text = "Text";
}
